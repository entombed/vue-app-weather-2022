import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '../store'

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'AddCity',
    meta: { protected: false },
    component: () => import('../views/AddCity.vue'),
  },
  {
    path: '/WeatherInCity/:cityName',
    name: 'WeatherInCity',
    meta: { protected: true },
    component: () => import('../views/WeatherInCity.vue'),
  },
  {
    path: '/:catchAll(.*)',
    redirect: '/'
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const isProtectedRoute = to.matched.some((item) => item.meta.protected);
  const cities = store.getters.cities;
  if (isProtectedRoute && !cities.length) {
    next({ name: 'AddCity' });
    return
  }
  next();
})

export default router;
