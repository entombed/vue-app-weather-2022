import Vuex from 'vuex'
import Vue from 'vue'
import { collection, getDocs, doc, updateDoc, addDoc, query, where, deleteDoc } from 'firebase/firestore';
import db from '../firebase/firebaseinit';
import axios from 'axios';

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    isLoadingAppPage: true,
    cities: null,
    selectedCity: null,
    API_KEY: process.env.VUE_APP_OPEN_WEATHER_API_KEY,
    API_URL: process.env.VUE_APP_OPEN_WEATHER_API_URL,
    sunPosition: null
  },
  getters: {
    cities: (s) => s.cities,
    selectedCity: (s) => s.selectedCity,
    getCityByName: (s) => (name) => {
      return s.cities.find((item) => item.city.toLowerCase()  === name.toLowerCase() ) 
    },
    sunPosition: (s) => s.sunPosition,
    isLoadingAppPage: (s) => s.isLoadingAppPage,
  },
  mutations: {
    updateCities(state, cities) {
      state.cities = cities;
    },
    addCity(state, data) {
      state.cities = [...state.cities, data];
    },
    deleteCity(state, city) {
      state.cities = state.cities.filter((item) => item.city !== city);
    },
    updateSelectedCity(state, city) {
      state.selectedCity = city;
    },
    mutateSunPosition(state, time) {
      state.sunPosition = time;
    },
    mutateIsLoadingAppPage(state, flag) {
      state.isLoadingAppPage = flag;
    }
  },
  actions: {
    actionCheckSunPosition({getters, commit}) {
      const dateObject = new Date();
      const currentTime = dateObject.getHours();
      const sunrise = new Date(getters.selectedCity.current.sunrise * 1000).getHours();
      const sunset = new Date(getters.selectedCity.current.sunset * 1000).getHours();
      if (currentTime > sunrise && currentTime < sunset) {
        commit('mutateSunPosition', 'isDay')
      } else {
        commit('mutateSunPosition', 'isNight')
      }
    },

    async fetchWeatherInCity({ state, getters, commit }, cityName) {
      try {
        const cityData = getters.getCityByName(cityName)
        const excludes = ['minutely', 'alerts']
        const {
          lat,
          lon
        } = cityData.currentWeather.coord;
        const response = await axios.get(
          `${state.API_URL}/onecall?lat=${lat}&lon=${lon}&exclude=${excludes.join(',')}&units=metric&appid=${state.API_KEY}`
        )
        commit('updateSelectedCity', response.data)
      } catch (error) {
        console.error(error.message)
      }
    },

    async deleteCityFromBase({ commit }, city) {
      try {
        const {
          city: name,
        } = city
        const citiesRef = collection(db, 'cities');
        const queryResult = query(citiesRef, where('city', '==', name));
        const querySnapshot = await getDocs(queryResult);
        querySnapshot.forEach(async (document) => {
          const { id } = document;
          await deleteDoc(doc(db, 'cities', id));
        });
        commit('deleteCity', name)
      } catch (error) {
        console.error(error.message)
      }
    },

    async fetchWeatherInCities({ commit, state }) {
      commit('mutateIsLoadingAppPage', true);
      const cities = await getDocs(collection(db, 'cities'));
      const cityList = [];
      try {
        cities.forEach(async (item) => {
          const city = item.data().city;
          const response = await axios.get(
            `${state.API_URL}/weather?q=${city}&units=metric&APPID=${state.API_KEY}`
          );
          const data = response.data;
          const documentRef = doc(db, 'cities', item.id);
          await updateDoc(documentRef, {
            currentWeather: data,
          });
          cityList.push(item.data());
        });
        commit('updateCities', cityList)
        commit('mutateIsLoadingAppPage', false);
      } catch (error) {
        console.error(error.message);
      }
    },

    async addCity({ commit, state }, cityName) {
      try {
        const response = await axios.get(
          `${state.API_URL}/weather?q=${cityName.toLowerCase()}&units=metric&APPID=${state.API_KEY}`
        );
        const updateData = {
          city: cityName,
          currentWeather: response.data
        };
        await addDoc(collection(db, 'cities'), updateData);
        commit('addCity', updateData);
      } catch (error) {
        alert(error.message)
      }
    }
  }
})

export default store;