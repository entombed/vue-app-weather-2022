import { initializeApp } from 'firebase/app';
import { getFirestore } from 'firebase/firestore';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: 'AIzaSyCAE2Xpyqv1WID4p16GQqu8y6FVNXk9GHA',
  authDomain: 'vue-app-weather-2022.firebaseapp.com',
  projectId: 'vue-app-weather-2022',
  storageBucket: 'vue-app-weather-2022.appspot.com',
  messagingSenderId: '380134457059',
  appId: '1:380134457059:web:6ca3bda45d69b57787cfd6'
};

const firebaseApp = initializeApp(firebaseConfig);

export default getFirestore(firebaseApp);